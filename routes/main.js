var express = require("express"),
    router = express.Router(),
    async = require("async"),
    directoryReader = require(__dirname + "/../lib/directoryReader");
    fileReader = require(__dirname + "/../lib/fileReader");

router.get("/", function(req, res) {
    var response = [];

    directoryReader.read(function(files) {
        async.each(files, function(file, callback) {
            fileReader.read(file, function(data) {
                response.push(data);

                callback();
            });
        }, function(err){
            if (err) throw err;

            res.json(response);
        });
    });
});

module.exports = router;