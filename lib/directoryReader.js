var fs = require("fs"),
    fileReader = require(__dirname + "/../lib/fileReader");

var directoryReader = {
    read: function(callback) {
        var self = this;

        fs.readdir("/etc/apache2/sites-available", function (err, files) {
            if (err) throw err;

            var output = [];
            for(f in files) {
                output.push(files[f]);
            }
            callback(output);
        });
    }
};

module.exports = directoryReader;