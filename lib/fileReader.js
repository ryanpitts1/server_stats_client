var fs = require("fs");

var fileReader = {
    read: function(file, callback) {
        fs.readFile("/etc/apache2/sites-available/" + file, { encoding: "utf8" }, function (err, data) {
            if (err) throw err;

            var vHostStats = ["servername", "documentroot", "errorlog"],
                output = {
                    "servername" : "",
                    "documentroot" : "",
                    "errorlog" : ""
                },
                lines = data.toString().split("\n");

            for(l in lines) {
                var words = lines[l].split(" ");
                for(w in words) {
                    if (vHostStats.indexOf(words[w].toLowerCase()) !== -1) {
                        var key = words[w].toLowerCase();

                        w++;
                        output[key] = words[w];
                    }
                }
            }
            callback(output);
        });
    }
};

module.exports = fileReader;