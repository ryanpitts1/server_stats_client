// Setup server variables
var express = require("express"),
    fs = require("fs"),
    app = express(),
    router = express.Router(),
    routes = require("./routes/main");


// Setup router and define ruotes
app.use("/api/v1", router);

router.use(function(req, res, next) {
    console.log("Getting server stats...");
    next();
});

// Setup router and define ruotes
app.use("/api/v1", routes);


// Start server
var server = app.listen(4000, function() {
    console.log("Server started!");
});